from django.contrib import admin
from app.internal.admin.admin_user import AdminUserAdmin
from .models import TelegramUser


admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"

@admin.register(TelegramUser)
class PersonAdmin(admin.ModelAdmin):
    pass