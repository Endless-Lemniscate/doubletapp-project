from django.core.management.base import BaseCommand
from app.internal.bot import TelegramBot
from config.settings import env


class Command(BaseCommand):
    def handle(self, *args, **options):
        TELEGRAM_BOT_API_KEY = env.str('TELEGRAM_BOT_API_KEY')
        bot = TelegramBot(token=TELEGRAM_BOT_API_KEY)
        bot.start()