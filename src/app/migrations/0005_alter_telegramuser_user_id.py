# Generated by Django 4.0.2 on 2022-02-05 21:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_alter_telegramuser_first_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telegramuser',
            name='user_id',
            field=models.BigAutoField(primary_key=True, serialize=False, unique=True),
        ),
    ]
