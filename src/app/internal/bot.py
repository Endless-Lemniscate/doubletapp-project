from click import command
from app.internal.models.telegram_user import TelegramUser
from telegram.ext import Updater
from telegram import Update
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from app.internal.services.add_phone_number_for_user_service import AddPhoneNumberForUserService 
from app.internal.services.upsert_user_service import UpsertUserService
from app.internal.services.is_user_exists_in_db_service import IsUserExistsInDbService
from app.internal.services.get_user_service import GetUserService
from app.internal.transport.rest.serializers import TelegramUserSerializer


class TelegramBot:
    default_handlers = list()
    available_commands = ""
    token = ""

    def __init__(self, token):
        self.token = token

    def start(this):
        updater = Updater(token=this.token, use_context=True)
        dispatcher = updater.dispatcher

        def start(update: Update, context: CallbackContext):
            is_created = UpsertUserService.execute(update._effective_user)
            if(is_created):
                context.bot.send_message(chat_id=update.effective_chat.id, text="Your profile successfully added to db")
            else:
                context.bot.send_message(chat_id=update.effective_chat.id, text="Your profile successfully updated")

        def set_phone(update: Update, context: CallbackContext):
            is_user_exists = IsUserExistsInDbService.execute(update.effective_chat.id)
            if(not is_user_exists):
                context.bot.send_message(chat_id=update.effective_chat.id, text="You first need to execute /start command")
                return

            context.bot.send_message(chat_id=update.effective_chat.id, text="Enter your phone number in format +79125552368:")

            def echo(update: Update, context: CallbackContext):
                user = update._effective_user
                added = AddPhoneNumberForUserService.execute(user.id, update.message.text)
                if(added):
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Phone number successfully added!")
                else:
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Wrong format. Please pass /set_phone command again")
                dispatcher.remove_handler(phone_number_handler)
                this._add_all_default_handlers(dispatcher)
            
            this._remove_all_default_handlers(dispatcher)
            phone_number_handler = MessageHandler(Filters.text & (~Filters.command), echo)
            dispatcher.add_handler(phone_number_handler)

        def default_command_handler(update: Update, context: CallbackContext):
            message = "Sorry. This bot is kinda dumb at this time and for now it can process only following commands: \n" + this.available_commands
            context.bot.send_message(chat_id=update.effective_chat.id, text=message)

        def default_message_handler(update: Update, context: CallbackContext):
            message = "Please, run one of the following commands: \n" + this.available_commands
            context.bot.send_message(chat_id=update.effective_chat.id, text=message)

        def me(update: Update, context: CallbackContext):
            is_user_exists = IsUserExistsInDbService.execute(update.effective_chat.id)
            if(is_user_exists):
                user = GetUserService.execute(update.effective_chat.id)
                serialized_user = TelegramUserSerializer(instance=user).data
                context.bot.send_message(chat_id=update.effective_chat.id, text=serialized_user)
            else:
                context.bot.send_message(chat_id=update.effective_chat.id, text="Command not allowed. Please execute /start command first")
        
        this._add_command_handler(CommandHandler('start', start))
        this._add_command_handler(CommandHandler('set_phone', set_phone))
        this._add_command_handler(CommandHandler('me', me))
        this.default_handlers.append(MessageHandler(Filters.command, default_command_handler))
        this.default_handlers.append(MessageHandler(Filters.text, default_message_handler))

        this._add_all_default_handlers(dispatcher)

        updater.start_polling()
        print("Telegram bot is ready to process incoming messages \nPress CTRL+C to exit")
        updater.idle()


    def _add_command_handler(this, command_handler: CommandHandler):
            this.available_commands += "/" + str(command_handler.command[0]) + "\n"
            this.default_handlers.append(command_handler)


    def _add_all_default_handlers(this, dispatcher):
        for handler in this.default_handlers:
            dispatcher.add_handler(handler)


    def _remove_all_default_handlers(this, dispatcher):
        for handler in this.default_handlers:
            dispatcher.remove_handler(handler)