from app.internal.models.telegram_user import TelegramUser
from phonenumber_field.modelfields import PhoneNumber


# This service is intended to add phone number to db entry of existsting user.
# It takes unique telegram user id as first parameter (pk in db) and phone 
# number string.
# It returns True if phone was added successfuly and returns False if something 
# went wrong, for example - not valid format of number was passed
class AddPhoneNumberForUserService:
    def execute(id, phone_number):
        try:
            normalized = PhoneNumber.from_string(phone_number=phone_number, region='RU')
        except:
            return False

        is_valid = normalized.is_valid()
        if(not is_valid):
            return False
        else:
            TelegramUser.objects.filter(user_id=id).update(phone_number=normalized.as_e164)
            return True