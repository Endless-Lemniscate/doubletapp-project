from math import fabs
from app.internal.models.telegram_user import TelegramUser


# This service returns user from db
class GetUserService:
    def execute(id):
        return TelegramUser.objects.get(user_id=id)