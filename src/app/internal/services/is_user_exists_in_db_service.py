from math import fabs
from app.internal.models.telegram_user import TelegramUser


# This service is simply checks if user already persist in db or not.
# It only takes unique telegram user id as first parameter (pk in db) 
# It returns True if user with passed id is exist and False if not
class IsUserExistsInDbService:
    def execute(id):
        try:
            TelegramUser.objects.get(user_id=id)
            return True
        except:
            return False