from app.internal.models.telegram_user import TelegramUser


# This service is take User object from telegram-bot library as argument and 
# writes all available fields to correspondent db table. 
# It returns True if user was added first time and returns False if user
# was updated
class UpsertUserService:
    def execute(user):
        user, created = TelegramUser.objects.update_or_create(
            user_id = user.id,
            defaults={
                "first_name": user.first_name,
                "last_name": user.last_name,
                "username": user.username,
                "language_code": user.language_code
            }
        )
        if(created):
            print("User created. Total user count=" + str(TelegramUser.objects.count()))
            
        return created