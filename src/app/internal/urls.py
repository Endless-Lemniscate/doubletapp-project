from django.urls import path
from app.internal.transport.rest.handlers import TelegramUsersListAPIView
from app.internal.transport.rest.handlers import TelegramUserAPIView


urlpatterns = [
    path('users', TelegramUsersListAPIView.as_view()),
    path('me/<int:pk>', TelegramUserAPIView.as_view()),
]
