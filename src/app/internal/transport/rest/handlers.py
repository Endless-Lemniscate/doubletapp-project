from app.internal.models.telegram_user import TelegramUser
from rest_framework import generics
from app.internal.transport.rest.serializers import TelegramUserSerializer


class TelegramUsersListAPIView(generics.ListAPIView):
    serializer_class = TelegramUserSerializer

    def get_queryset(self):
        return TelegramUser.objects.all()


class TelegramUserAPIView(generics.RetrieveAPIView):
    serializer_class = TelegramUserSerializer

    def get_queryset(self):
        return TelegramUser.objects.all()