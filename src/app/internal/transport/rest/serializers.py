from rest_framework import serializers
from app.internal.models.telegram_user import TelegramUser


class TelegramUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = TelegramUser
        fields = ['user_id', 'first_name', 'last_name', 'username', 'language_code', 'phone_number']
