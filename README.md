# Template for backend course from DoubleTapp
This is test work for submission to DoubleTapp course for junior backend developers

## How to use

### Add telegram bot api key
First you need to create .env file in /src/config and add in it your TG api key as stated in file .env.example

### Start django server
To run django admin panel you need to execute following command in /src folder:
```
python manage.py runserver
```
Admin panel now available at http://127.0.0.1:8000/admin/
Server has only two endpoints: 
* **api/me/<tg-user-id>** which returns json object of telegram user with specified id.  
* **api/users** - returns list of all telegram user from local db in json format (this one do not support paging, tbh)

### Start bot handler
To run bot you need to execute following command in /src folder:
```
python manage.py start_bot
```
Great! Now your bot can handle following commands:

* **/start** - adds inforamtion about user to local db
* **/set_phone** - adds phone number to user entity in db
* **/me** - returns to user information about him stored in local db